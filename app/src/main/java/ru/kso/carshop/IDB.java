package ru.kso.carshop;

public interface IDB {
    String ANDROID_PATH = "file:///android_asset/";
    String DB_NAME = "cars";
    String TABLE_BRAND = "car_brands";
    String TABLE_CARS = "car_table";
    String TABLE_ACCOUNTS = "accounts_table";
    String KEY_BRAND = "brand";
    String KEY_LOGO = "logo";
    String KEY_MODEL = "model";
    String KEY_MODEL_IMG = "model_img";
    String KEY_LOGIN = "login";
    String KEY_PASSWORD = "password";

}
