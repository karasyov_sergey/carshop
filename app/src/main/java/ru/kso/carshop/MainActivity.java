package ru.kso.carshop;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private SQLiteDatabase mDatabase;
    private boolean mSuccessful;
    private boolean mFreeAcc;
    private EditText inputLogin;
    private EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        Button btnAuthorization = findViewById(R.id.btnAuthorization);
        Button btnRegistration = findViewById(R.id.btnRegistration);
        inputLogin = findViewById(R.id.inputLogin);
        inputPassword = findViewById(R.id.inputPassword);
        DBHelper mDBHelper = new DBHelper(MainActivity.this);
        mDatabase = mDBHelper.getWritableDatabase();
        mSuccessful = false;
        mFreeAcc = true;

        btnAuthorization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = inputLogin.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                if (login.isEmpty()) {
                    inputLogin.setError("Введите логин");
                    return;
                }
                if (password.isEmpty()) {
                    inputPassword.setError("Введите пароль");
                    return;
                }
                Cursor cursor = mDatabase.query(DBHelper.TABLE_ACCOUNTS, null,
                        null, null, null,
                        null, null);
                if (cursor.moveToFirst()) {
                    int loginIndex = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                    int passwordIndex = cursor.getColumnIndex(DBHelper.KEY_PASSWORD);
                    do {
                        if (login.equals(cursor.getString(loginIndex)) &&
                                password.equals(cursor.getString(passwordIndex))) {
                            mSuccessful = true;
                            break;
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
                if (mSuccessful) {
                    initBrandList();
                } else {
                    inputLogin.setError("Неверные данные");
                    inputPassword.setError("Неверные данные");
                }
            }
        });
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = inputLogin.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                if (login.length() < 4) {
                    inputLogin.setError("Логин должен содержать не менее 4 символов");
                    return;
                }

                if (password.length() < 4) {
                    inputPassword.setError("Пароль должен содержать не менее 4 символов");
                    return;
                }

                if (login.contains(" ")) {
                    inputLogin.setError("Недопустимый формат для логина");
                    return;
                }

                if (password.contains(" ")) {
                    inputPassword.setError("Недопустимый формат для пароля");
                    return;
                }

                if (password.equalsIgnoreCase(login)) {
                    inputPassword.setError("Пароль должен быть отличным от логина");
                    return;
                }

                Cursor cursor = mDatabase.query(DBHelper.TABLE_ACCOUNTS,
                        new String[]{DBHelper.KEY_LOGIN},
                        null, null, null,
                        null, null);
                if (cursor.moveToFirst()) {
                    int loginIndex = cursor.getColumnIndex(DBHelper.KEY_LOGIN);
                    do {
                        if (login.equals(cursor.getString(loginIndex))) {
                            mFreeAcc = false;
                            break;
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
                if (mFreeAcc) {
                    ContentValues values = new ContentValues();
                    values.put(DBHelper.KEY_LOGIN, login);
                    values.put(DBHelper.KEY_PASSWORD, password);
                    mDatabase.insert(DBHelper.TABLE_ACCOUNTS, null, values);
                    initBrandList();
                } else {
                    inputLogin.setError("Пользователь с таким логином уже существует");
                }
            }
        });
    }


    private void initBrandList() {
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.brands);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager =
                new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        CarBrandAdapter carBrandAdapter = new CarBrandAdapter(MainActivity.this);
        recyclerView.setAdapter(carBrandAdapter);
    }


    public class CarBrandAdapter extends RecyclerView.Adapter<CarBrandAdapter.BrandHolder> {

        private Context context;

        private ArrayList<String> logo;
        private ArrayList<String> brand;

        CarBrandAdapter(Context context) {
            this.context = context;
            initData();

        }

        @NonNull
        @Override
        public BrandHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.cell_brand, parent, false);
            return new BrandHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BrandHolder holder, int position) {
            setImageFromPath(logo.get(position), holder.img);
            setTextFromDB(brand.get(position), holder.textView);
        }

        private void setTextFromDB(String s, TextView textView) {
            textView.setText(s);
        }


        private void setImageFromPath(String path, ImageView image) {
            Glide.with(context).asBitmap().
                    load(path).
                    into(image);
        }

        private void initData() {
            logo = new ArrayList<>();
            brand = new ArrayList<>();
            Cursor cursor = mDatabase.query(DBHelper.TABLE_BRAND, null,
                    null, null, null, null, null);
            int logoIndex = cursor.getColumnIndex(DBHelper.KEY_LOGO);
            int brandIndex = cursor.getColumnIndex(DBHelper.KEY_BRAND);
            while (cursor.moveToNext()) {
                logo.add(cursor.getString(logoIndex));
                brand.add(cursor.getString(brandIndex));
            }
            cursor.close();
        }

        @Override
        public int getItemCount() {
            return logo.size();
        }

        class BrandHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private ImageView img;
            private TextView textView;

            BrandHolder(@NonNull View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.brand_logo);
                textView = itemView.findViewById(R.id.brand_name);
                img.setOnClickListener(this);
                textView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,
                        CarModelsActivity.class);
                intent.putExtra("brand", textView.getText().toString());
                startActivity(intent);
            }
        }
    }

}

