package ru.kso.carshop;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CarModelsActivity extends AppCompatActivity {
    private SQLiteDatabase mDatabase;
    private ArrayList<String> models;
    private ArrayList<String> modelImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intent_activity);
        DBHelper mDBHelper = new DBHelper(this);
        mDatabase = mDBHelper.getWritableDatabase();
        initData();
        RecyclerView recyclerView = findViewById(R.id.cars);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager =
                new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        CarAdapter carAdapter = new CarAdapter(this, modelImages, models);
        recyclerView.setAdapter(carAdapter);
    }

    private void initData() {
        models = new ArrayList<>();
        modelImages = new ArrayList<>();
        Intent intent = getIntent();

        Cursor cursor = mDatabase.query(DBHelper.TABLE_CARS,
                new String[]{DBHelper.KEY_MODEL, DBHelper.KEY_MODEL_IMG},
                DBHelper.KEY_BRAND + "= ?",
                new String[]{intent.getStringExtra("brand")},
                null, null, null);
        if (cursor.moveToFirst()) {
            int modelIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL);
            int imageIndex = cursor.getColumnIndex(DBHelper.KEY_MODEL_IMG);
            do {
                models.add(cursor.getString(modelIndex));
                modelImages.add(cursor.getString(imageIndex));
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    public class CarAdapter extends RecyclerView.Adapter<CarAdapter.CarHolder> {
        private Context context;
        private ArrayList<String> images;
        private ArrayList<String> models;

        CarAdapter(Context context, ArrayList<String> images, ArrayList<String> models) {
            this.context = context;
            this.images = images;
            this.models = models;
        }

        @NonNull
        @Override
        public CarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.cell_car, parent, false);
            return new CarHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CarHolder holder, int position) {
            setImageFromPath(images.get(position), holder.img);
            setTextFromDB(models.get(position), holder.textView);
        }

        private void setTextFromDB(String s, TextView textView) {
            textView.setText(s);
        }


        private void setImageFromPath(String path, ImageView image) {
            Glide.with(context).asBitmap().
                    load(path).
                    into(image);
        }


        @Override
        public int getItemCount() {
            return models.size();
        }

        class CarHolder extends RecyclerView.ViewHolder {
            private ImageView img;
            private TextView textView;

            CarHolder(@NonNull View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.image_model);
                textView = itemView.findViewById(R.id.model_name);
            }
        }
    }
}
