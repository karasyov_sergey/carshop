package ru.kso.carshop;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.io.IOException;

public class DBHelper extends SQLiteOpenHelper implements IDB {
    private Context context;
    private AssetManager manager;

    DBHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        manager = context.getAssets();
        db.execSQL("create table " + TABLE_BRAND + " ( " + KEY_BRAND + " text primary key, "
                + KEY_LOGO + " text ) ");
        db.execSQL("create table " + TABLE_CARS + " ( " + KEY_MODEL + " text primary key, "
                + KEY_BRAND + " text, " + KEY_MODEL_IMG + " text )");
        db.execSQL("create table " + TABLE_ACCOUNTS + " ( " + KEY_LOGIN + " text primary key, "
                + KEY_PASSWORD + " text )");
        initBrandTable(db);
        initCarTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void initBrandTable(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        try {
            String[] files = manager.list("logo");
            if (files != null) {
                for (String file : files) {
                    values.put(KEY_BRAND, file.substring(0, file.lastIndexOf('.')));
                    values.put(KEY_LOGO, ANDROID_PATH + file);
                    db.insert(TABLE_BRAND, null, values);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initCarTable(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        try {
            String[] files = manager.list("model");
            if (files != null) {
                for (String file : files) {
                    values.put(KEY_BRAND, file.substring(0, file.indexOf(' ')));
                    values.put(KEY_MODEL, file.substring(0, file.lastIndexOf('.')));
                    values.put(KEY_MODEL_IMG, ANDROID_PATH + file);
                    db.insert(TABLE_CARS, null, values);
                }
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

}
